from copy import copy

import cv2
import os

from DetermineValueBlur import DetermineValueBlur


blurClass = DetermineValueBlur()

def out_image(image):
    imageAnswer, num = blurClass.determineValueBlur(image, 40)

    cv2.imshow("laplacian",
               resize_image(imageAnswer, 500))


def resize_image(image, width=None, height=None):
    heightImage, widthImage = image.shape[:2]

    if width is None and height is None:
        return image
    if width is None:
        factor = height / heightImage
        dim = (int(widthImage * factor), height)
    else:
        factor = width / widthImage
        dim = (width, int(heightImage * factor))
    return cv2.resize(image, dim)


def setThreshold(value):
    blurClass.Threshold = value
    out_image(copy(image))


cv2.namedWindow('laplacian')
imageNumber = 283
isWork = True
path = 'dataSet'
listDir = os.listdir(path)
listDirSize = len(listDir)
image = cv2.imread(os.path.join(path, listDir[imageNumber]))
out_image(copy(image))
cv2.createTrackbar('threshold', 'laplacian', 0, 255, setThreshold)
cv2.setTrackbarPos('threshold', 'laplacian', 197)

while isWork:
    if cv2.waitKey(1) == 27:
        isWork = False

    if cv2.waitKey(33) == ord('d'):
        imageNumber += 1
        if imageNumber >= listDirSize:
            imageNumber = 0
        image = cv2.imread(os.path.join(path, listDir[imageNumber]))
        out_image(copy(image))
        print(imageNumber)

    if cv2.waitKey(33) == ord('a'):
        imageNumber -= 1
        if imageNumber < 0:
            imageNumber = listDirSize - 1
        image = cv2.imread(os.path.join(path, listDir[imageNumber]))
        out_image(copy(image))
        print(imageNumber)
